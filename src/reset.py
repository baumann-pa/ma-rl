#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
from geometry_msgs.msg import Pose
from tf.transformations import quaternion_from_euler

set_state = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)


def _reset_model_pos():
    pose = Pose()
    pose.position.x = 4.003439
    pose.position.y = 0.806448
    pose.position.z = 0.358248

    q = quaternion_from_euler(0.0, 0.0, 0.0)  # roll, pitch, yaw

    pose.orientation.x = q[0]
    pose.orientation.y = q[1]
    pose.orientation.z = q[2]
    pose.orientation.w = q[3]

    state = ModelState()
    state.model_name = 'racecar'
    state.pose = pose

    try:
        set_state(state)
    except rospy.ServiceException as e:
        raise rospy.ServiceException("Service call failed: %s" % (e,))


if __name__ == '__main__':
    _reset_model_pos()
