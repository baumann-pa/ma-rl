#!/usr/bin/env python
from json import load
from os import path
from posixpath import normpath
from sys import argv

from core.Agent import Agent
from core.DriveControl import DriveControlSimulation
from envs.GazeboEnv import GazeboEnv
from processors.GazeboProcessor import GazeboProcessor
from processors.ImageProcessor import ImageProcessor
from processors.VisualizationProcessor import VisualizationProcessor
from utils import file_writer


def main():
    # PARAMS
    config_env = 'mit_default'
    config_env_pos = 'mit_default_pos'  # Use None for single position
    config_agent = 'default_bgr'  # default_bgr/default_mono8
    config_img_processing = 'default_bgr'  # default_bgr/default_mono8
    config_env_processing = 'gazebo'
    config_run = 'default'

    # TODO use argparse
    # overwrite default config if args are parsed
    if argv[1:]:
        if argv[1:][0]:
            config_env = argv[1:][0]
        if argv[1:][1]:
            config_agent = argv[1:][1]
        if argv[1:][2]:
            config_img_processing = argv[1:][2]
        if argv[1:][3]:
            config_env_processing = argv[1:][3]
        if argv[1:][4]:
            config_run = argv[1:][4]

    tmp_dir = path.dirname(__file__)
    file_path = "configs/run/training/%s.json" % (config_run,)
    file_path = normpath(path.join(tmp_dir, file_path))
    if path.isfile(file_path):
        with open(file_path) as data_file:
            config_run = load(data_file)
    else:
        raise IOError('No such file: %s' % (file_path,))

    file_path = "configs/env_processing/%s.json" % (config_env_processing,)
    file_path = normpath(path.join(tmp_dir, file_path))
    if path.isfile(file_path):
        with open(file_path) as data_file:
            config_env_processing = load(data_file)
    else:
        raise IOError('No such file: %s' % (file_path,))

    nb_steps = config_run['nb_steps']
    nb_max_episode_steps = config_run['nb_max_episode_steps']
    nb_episodes_test = config_run['nb_episodes_test']
    action_repetition = config_run['action_repetition']

    verbose = config_run['log']['verbose']  # show log while training
    log_performance = config_run['log']['performance']

    image_processor = ImageProcessor(config=config_img_processing,
                                     config_agent=config_agent)

    env_processor = GazeboProcessor(image_processor=image_processor,
                                    record=config_env_processing['record']['image'])

    agent_class = Agent(config=config_agent,
                        image_processor=image_processor,
                        processor=env_processor,
                        learning_rate_actor=config_run["learning_rate_actor"],
                        learning_rate_critic=config_run["learning_rate_critic"],
                        verbose=verbose['agent'])

    visualizer = VisualizationProcessor(visualize=config_env_processing['record']['prediction'],
                                        actor=agent_class.agent.actor)

    env_processor.set_visualizer(visualizer)

    drive_control = DriveControlSimulation(max_speed=config_run['max_speed'])

    gazebo = GazeboEnv(config=config_env,
                       action_space=agent_class.action_space,
                       observation_space=agent_class.observation_space,
                       drive_control=drive_control,
                       reset_positions=config_env_pos,
                       verbose=verbose['environment'])

    history = agent_class.train(env=gazebo,
                                nb_max_episode_steps=nb_max_episode_steps,
                                nb_steps=nb_steps,
                                nb_episodes_test=nb_episodes_test,
                                action_repetition=action_repetition,
                                verbose=verbose['train'])

    if log_performance:
        file_writer.write_log_training(agent_class.name, history)
        file_writer.write_log_graphs(agent_class.name, history)


if __name__ == '__main__':
    main()
