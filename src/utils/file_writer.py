#!/usr/bin/env python
from cv2 import imwrite
from os import makedirs, path
from posixpath import normpath
from threading import Lock
from time import time, localtime, strftime

import matplotlib.pyplot as plt
from keras.utils.vis_utils import plot_model

lock = Lock()


def write_nn_structure(model, name):
    tmp_dir = path.dirname(__file__)
    file_path = "graphs/%s" % (name,)
    file_path = path.join(tmp_dir, "../..", file_path)
    file_path = normpath(file_path)
    if not path.exists(file_path):
        makedirs(file_path)
    file_path = "%s/%s_nn_structure.png" % (file_path, name,)
    file_path = normpath(file_path)
    plot_model(model, to_file=file_path, show_shapes=True, show_layer_names=True)


def write_log_text(name, log_text, mode):
    tmp_dir = path.dirname(__file__)
    file_path = "logs/%s" % (name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))

    lock.acquire()

    if not path.exists(file_path):
        makedirs(file_path)

    tmp_time = strftime("%Y-%m-%d", localtime())
    file_path = normpath("%s/%s_%s.log" % (file_path, tmp_time, mode))

    with open(file_path, "a") as tmp_file:
        tmp_file.write("{}\n".format(log_text))

    lock.release()


def write_log_training(name, history):
    tmp_dir = path.dirname(__file__)
    file_path = "logs/%s" % (name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))
    if not path.exists(file_path):
        makedirs(file_path)
    tmp_time = strftime("%Y-%m-%d %H:%M:%S", localtime())
    file_path = normpath("%s/%s.log" % (file_path, tmp_time,))

    for nb_epoch in history.epoch:
        log_data = "epoch: %s | reward: %s - steps: %s" % (
            str(nb_epoch + 1),
            str(history.history['episode_reward'][nb_epoch]),
            str(history.history['nb_steps'][nb_epoch])
        )
        with open(file_path, "a") as log_text:
            log_text.write("{}\n".format(log_data))


def write_log_graphs(name, history):
    tmp_dir = path.dirname(__file__)
    file_path = "logs/%s" % (name,)
    file_path = normpath(path.join(tmp_dir, "../..", file_path))
    if not path.exists(file_path):
        makedirs(file_path)

    # path for accuracy log
    tmp_time = strftime("%Y-%m-%d %H:%M:%S", localtime())

    # path for reward log
    reward = "%s/%s_reward.png" % (file_path, tmp_time,)
    reward = normpath(reward)

    # summarize history for reward
    plt.plot(history.history['episode_reward'])
    reward_title = "%s Reward" % (name,)
    plt.title(reward_title)
    plt.ylabel('reward')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig(reward)


def write_image(image, folder_name, file_name=None):
    # log image
    tmp_dir = path.dirname(__file__)
    file_path = path.join(tmp_dir, "../..", str(folder_name))

    if not file_name:
        file_name = int(round(time() * 1000))
    else:
        file_name = file_name

    file_path = "%s/%s.png" % (file_path, str(file_name),)
    file_path = normpath(file_path)
    imwrite(file_path, image)

    return file_name
