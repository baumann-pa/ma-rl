#!/usr/bin/env python
from __future__ import division

from json import load
from os import path
from posixpath import normpath

import numpy as np
import rospy
from GazeboTransformListener import GazeboTransformListener
from ackermann_msgs.msg import AckermannDriveStamped
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import GetModelState, SetModelState
from geometry_msgs.msg import Pose
from gym import Env
from laser_geometry import LaserProjection
from scipy.stats import linregress
from sensor_msgs.msg import CompressedImage, LaserScan
from sensor_msgs.point_cloud2 import read_points
from std_srvs.srv import Empty as EmptySrv
from tf.transformations import quaternion_from_euler
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud

FPS = 30  # frames per second
COLLISION_RANGE = 0.25
MESSAGE_TIMEOUT = 5


class GazeboEnv(Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, config, action_space, observation_space, drive_control, reset_positions=None, verbose=0):
        # Agent settings
        tmp_dir = path.dirname(__file__)
        filename = "configs/env/%s.json" % (config,)
        filename = normpath(path.join(tmp_dir, "..", filename))
        if path.isfile(filename):
            with open(filename) as data_file:
                self.config = load(data_file)
        else:
            raise IOError('No such file: %s' % (filename,))

        if reset_positions:
            # Random start positions
            filename = "configs/env/%s.json" % (reset_positions,)
            filename = normpath(path.join(tmp_dir, "..", filename))
            if path.isfile(filename):
                with open(filename) as data_file:
                    reset_positions = load(data_file)
            else:
                raise IOError('No such file: %s' % (filename,))

        self.env_name = self.config["env_name"]
        self.positions_random_flip_yaw = self.config["pos_random_flip_yaw"]
        self.positions = self._create_positions(reset_positions)
        self.action_space = action_space
        self.observation_space = observation_space
        self.reward_range = [-np.inf, np.inf]
        self.drive_control = drive_control
        self.min_range = COLLISION_RANGE
        self.laser_projection = LaserProjection()
        self.transform_lister = GazeboTransformListener()
        self.laser_data = None
        self.transform = None
        self.verbose = verbose

        # init node
        rospy.init_node('gazebo_env')

        # publisher
        self.drive_control_publisher = rospy.Publisher("/vesc/ackermann_cmd_mux/input/navigation",
                                                       AckermannDriveStamped,
                                                       queue_size=20)

        # init services
        self.pause = rospy.ServiceProxy("/gazebo/pause_physics", EmptySrv)
        self.unpause = rospy.ServiceProxy("/gazebo/unpause_physics", EmptySrv)
        self.set_state = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)
        self.get_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)

        self._reset()

    def _calculate_reward(self, speed, racecar_p1, racecar_p2):
        if isinstance(self.laser_data, LaserScan):
            # calculate degrees per point
            points_per_degree = len(self.laser_data.ranges) / (
                np.degrees(abs(self.laser_data.angle_min)) +
                np.degrees(abs(self.laser_data.angle_max)))
            one_point = 1.0 / points_per_degree

            # Split into left and right sector and convert polar to cartesian form.
            # Since only 'nb_points' of each side is taken, the corresponding angle of each side
            # has to be adjusted, otherwise the conversion to cartesian will be wrong
            # left: adjust angle_max, right: adjust angle_min

            # right sector
            nb_points = 135
            angle_reduce = np.radians(one_point * nb_points)
            laser_data = LaserScan(
                header=self.laser_data.header,
                angle_min=self.laser_data.angle_min,
                angle_max=self.laser_data.angle_min + angle_reduce,
                angle_increment=self.laser_data.angle_increment,
                time_increment=self.laser_data.time_increment,
                scan_time=self.laser_data.scan_time,
                range_min=self.laser_data.range_min,
                range_max=self.laser_data.range_max,
                ranges=self.laser_data.ranges[:nb_points],
                intensities=self.laser_data.intensities[:nb_points]
            )

            point_cloud = self.laser_projection.projectLaser(laser_data)
            points_transform = do_transform_cloud(point_cloud, self.transform)
            points_cartesian = read_points(points_transform, field_names=("x", "y", "z"), skip_nans=True)

            right_data_x = []
            right_data_y = []
            for p in points_cartesian:
                right_data_x.append(p[0])
                right_data_y.append(p[1])

            if len(right_data_x) < 2 or len(right_data_y) < 2:
                return -1.0, True

            # left sector
            nb_points = len(self.laser_data.ranges) - nb_points
            laser_data = LaserScan(
                header=self.laser_data.header,
                angle_min=self.laser_data.angle_max - angle_reduce,
                angle_max=self.laser_data.angle_max,
                angle_increment=self.laser_data.angle_increment,
                time_increment=self.laser_data.time_increment,
                scan_time=self.laser_data.scan_time,
                range_min=self.laser_data.range_min,
                range_max=self.laser_data.range_max,
                ranges=self.laser_data.ranges[nb_points:],
                intensities=self.laser_data.intensities[nb_points:]
            )

            point_cloud = self.laser_projection.projectLaser(laser_data)
            points_transform = do_transform_cloud(point_cloud, self.transform)
            points_cartesian = read_points(points_transform, field_names=("x", "y", "z"), skip_nans=True)

            left_data_x = []
            left_data_y = []
            for p in points_cartesian:
                left_data_x.append(p[0])
                left_data_y.append(p[1])

            if len(left_data_x) < 2 or len(left_data_x) < 2:
                return -1.0, True

            # calculate slopes
            right_slope, right_intercept, right_r_value, right_p_value, right_std_err = linregress(
                right_data_x, right_data_y)

            left_slope, left_intercept, left_r_value, left_p_value, left_std_err = linregress(
                left_data_x, left_data_y)

            track_slope = right_slope / 2.0 + left_slope / 2.0

            racecar_slope, racecar_intercept, racecar_r_value, racecar_p_value, racecar_std_err = linregress(
                [racecar_p1[0], racecar_p2[0]], [racecar_p1[1], racecar_p2[1]])

            # theta = |(slope2 - slope1) / (1 + slope1 * slope2)|
            theta = np.arctan(np.abs((track_slope - racecar_slope) / (1.0 + track_slope * racecar_slope)))

            # maximize longitudinal velocity (first term), (deep mind original)
            # minimize transverse velocity (second term)  (extension from Ben Lau)
            reward = speed * np.cos(theta) - np.abs(speed * np.sin(theta))

            # Episode is terminated if the agent runs backward
            done = True if np.cos(theta) < 0 else False

            if self.verbose > 0:
                print ('car_slope: %s, track_slope: %s' % (racecar_slope, track_slope))
                print ('theta: %s' % (np.degrees(theta),))

            return reward, done
        else:
            raise ValueError('Expected type LaserScan. Got %s' % (type(self.laser_data),))

    def _close(self):
        self._stop_engine()

    def _create_positions(self, random_positions=None):
        if not random_positions:
            random_positions = {'pos_0': self.config["pos_reset"]}

        positions = []

        for position in random_positions.values():
            pose = Pose()
            pose.position.x = position["position"]["x"]
            pose.position.y = position["position"]["y"]
            pose.position.z = position["position"]["z"]

            q = quaternion_from_euler(position["orientation"]["roll"],
                                      position["orientation"]["pitch"],
                                      position["orientation"]["yaw"])

            pose.orientation.x = q[0]
            pose.orientation.y = q[1]
            pose.orientation.z = q[2]
            pose.orientation.w = q[3]

            positions.append(pose)

            # flip direction of car
            if self.positions_random_flip_yaw:
                pose = Pose()
                pose.position.x = position["position"]["x"]
                pose.position.y = position["position"]["y"]
                pose.position.z = position["position"]["z"]
                q = quaternion_from_euler(position["orientation"]["roll"],
                                          position["orientation"]["pitch"],
                                          position["orientation"]["yaw"] * -1.0)

                pose.orientation.x = q[0]
                pose.orientation.y = q[1]
                pose.orientation.z = q[2]
                pose.orientation.w = q[3]

                positions.append(pose)

        return positions

    @staticmethod
    def _get_image_data():
        try:
            return rospy.wait_for_message('/camera/zed/rgb/image_rect_color/compressed',
                                          CompressedImage,
                                          timeout=MESSAGE_TIMEOUT)
        except rospy.ROSException:
            raise rospy.ROSException('Camera not available.')

    def _get_laser_data(self):
        try:
            laser_data = rospy.wait_for_message('/scan',
                                                LaserScan,
                                                timeout=MESSAGE_TIMEOUT)
        except rospy.ROSException:
            raise rospy.ROSException('Scan not available.')

        try:
            transform = self.transform_lister.lookup_transform_msg('map', 'laser', rospy.Time(0),
                                                                   timeout=rospy.Duration(5))
        except:
            raise Exception('Error while getting transform msg')

        return laser_data, transform

    def _get_position(self):
        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            return self.get_state('racecar', 'world')
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

    def _collided(self):
        if isinstance(self.laser_data, LaserScan):
            for val in self.laser_data.ranges:
                if not np.isinf(val) and self.min_range > val:
                    return True
        else:
            raise ValueError('Expected type LaserScan. Got %s' % (type(self.laser_data),))

        return False

    def _reset(self):
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

        self._stop_engine()
        self._reset_model_pos()

        image_data = self._get_image_data()

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            self.pause()
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

        return image_data  # state

    def _reset_model_pos(self):
        idx = np.random.randint(0, len(self.positions))

        state = ModelState()
        state.model_name = 'racecar'
        state.pose = self.positions[idx]

        if self.verbose > 0:
            print("Moving robot")

        try:
            ret = self.set_state(state)
            if self.verbose > 0:
                print (ret.status_message)
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

    def _step(self, action):
        # make action
        steer = action[0]
        speed = 1.0  # action[1]  # TODO dimension bug with keras-rl for more than one dimension
        command = self.drive_control.create_command(steer, speed)

        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

        pre_action_position = self._get_position()

        self.drive_control_publisher.publish(command)

        # get laser and image data
        self.laser_data, self.transform = self._get_laser_data()
        image_data = self._get_image_data()

        post_action_position = self._get_position()

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            self.pause()
        except rospy.ServiceException as e:
            raise rospy.ServiceException("Service call failed: %s" % (e,))

        done = self._collided()

        if not done:
            p1 = (pre_action_position.pose.position.x, pre_action_position.pose.position.y)
            p2 = (post_action_position.pose.position.x, post_action_position.pose.position.y)

            reward, done = self._calculate_reward(speed, p1, p2)
        else:
            reward = -1.0

        # state = image_data
        return image_data, reward, done, {}

    def _stop_engine(self):
        if self.verbose > 0:
            print('Stopping engine')

        command = self.drive_control.stop_engine()
        self.drive_control_publisher.publish(command)
