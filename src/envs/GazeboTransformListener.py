#!/usr/bin/env python
from rospy import Duration
from tf import TransformListener
from tf.listener import strip_leading_slash


class GazeboTransformListener(TransformListener):
    def __init__(self):
        super(GazeboTransformListener, self).__init__()

    def lookup_transform_msg(self, target_frame, source_frame, time, timeout=Duration(0)):
        """
        Get the transform from the source frame to the target frame. The whole message will be returned
        instead of (translation, rotation) like the original lookupTransform function.


        :param target_frame: Name of the frame to transform into.
        :param source_frame: Name of the input frame.
        :param time: The time at which to get the transform. (0 will get the latest)
        :param timeout: (Optional) Time to wait for the target frame to become available.
        :return: The transform between the frames.
        :rtype: :class:`geometry_msgs.msg.TransformStamped`
        """
        return self._buffer.lookup_transform(strip_leading_slash(target_frame), strip_leading_slash(source_frame),
                                             time, timeout)
