#!/usr/bin/env python
from time import time

import numpy as np
from utils.file_writer import write_image
from vis.utils.utils import find_layer_idx
from vis.visualization import visualize_activation, visualize_saliency


class VisualizationProcessor:
    def __init__(self, visualize=False, actor=None):
        self.visualize = visualize
        self.actor = actor

    def visualize_activation(self, image, file_name=None, layer_name='prediction'):
        seed_input = np.array(image).reshape(1, image.shape[0], image.shape[1], image.shape[2]).astype('float32')

        layer_idx = find_layer_idx(self.actor, layer_name)

        img = visualize_activation(self.actor, layer_idx,
                                   filter_indices=None,
                                   backprop_modifier='guided',
                                   seed_input=seed_input)

        if not file_name:
            file_name = int(round(time() * 1000))
        else:
            file_name = file_name

        write_image(np.squeeze(img), 'vis/layer', '%s_%s' % (file_name, layer_name))

    def visualize_saliency(self, image, file_name=None, layer_name='prediction'):
        seed_input = np.array(image).reshape(1, image.shape[0], image.shape[1], image.shape[2]).astype('float32')

        layer_idx = find_layer_idx(self.actor, layer_name)

        img = visualize_saliency(self.actor, layer_idx,
                                 filter_indices=None,
                                 backprop_modifier='guided',
                                 seed_input=seed_input)

        if not file_name:
            file_name = int(round(time() * 1000))
        else:
            file_name = file_name

        write_image(np.squeeze(img), 'vis/layer', '%s_%s' % (file_name, layer_name))
