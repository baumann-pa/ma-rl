#!/usr/bin/env python
import numpy as np
from cv_bridge import CvBridge
from rl.core import Processor
from utils.file_writer import write_image


class GazeboProcessor(Processor):
    def __init__(self, image_processor, record=False):
        super(GazeboProcessor, self).__init__()
        self.image_processor = image_processor
        self.record = record
        self.cv_bridge = CvBridge()
        self.visualizer = None

    def set_visualizer(self, visualizer):
        self.visualizer = visualizer

    def process_observation(self, observation):
        cv_image = self.cv_bridge.compressed_imgmsg_to_cv2(observation,
                                                           self.image_processor.color_scale.encode('latin_1'))

        image_data = np.array(self.image_processor.resize_img(cv_image)).astype('float32')

        if self.image_processor.crop_image:
            image_data = np.array(self.image_processor.crop_img(image_data)).astype('float32')

        corresponding_img_name = None
        if self.record:
            corresponding_img_name = write_image(image_data, 'vis/raw', corresponding_img_name)

        image_data /= 255.
        image_data = self.image_processor.transform_img_for_backend(image_data)

        if self.visualizer and self.visualizer.visualize:
            # NOTE: debug Lillicrap
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug1')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug2')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug3')
            # # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'flattened_observation')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug5')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug6')

            # NOTE: debug NVIDIA
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug1')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug2')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug3')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug4')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug5')
            # # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'flattened_observation')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug7')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug8')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug9')
            # self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'debug10')

            # NOTE: use this in general
            self.visualizer.visualize_saliency(image_data, corresponding_img_name, 'prediction')

        return image_data
