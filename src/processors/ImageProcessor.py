#!/usr/bin/env python
from cv2 import resize, INTER_CUBIC
import json
from os import path
from posixpath import normpath

import numpy as np
from keras import backend as k


class ImageProcessor:
    def __init__(self, config, config_agent):
        # Settings
        tmp_dir = path.dirname(__file__)
        filename = "configs/img_processing/%s.json" % (config,)
        filename = normpath(path.join(tmp_dir, "..", filename))
        if path.isfile(filename):
            with open(filename) as data_file:
                config = json.load(data_file)
        else:
            raise IOError('No such file: %s' % (filename,))

        self.crop_image = config["crop_image"]
        self.crop_top = config["crop_top"]
        self.crop_bottom = config["crop_bottom"]

        # Raw image settings of agent
        filename = "configs/agent/%s.json" % (config_agent,)
        filename = normpath(path.join(tmp_dir, "..", filename))
        if path.isfile(filename):
            with open(filename) as data_file:
                config = json.load(data_file)
        else:
            raise IOError('No such file: %s' % (filename,))

        self.color_scale = config["color_scale"]
        self.img_depth = config["img_depth"]
        self.img_width = config["img_width"]
        self.img_height = config["img_height"]

    def get_cropped_height(self):
        start_y = int((float(self.img_height) / 100.00) * float(self.crop_top))
        end_y = int((float(self.img_height) / 100.00) * float(self.crop_bottom))

        return end_y - start_y

    def crop_img(self, image):
        # height
        height = image.shape[0]
        start_y = int((float(height) / 100.00) * float(self.crop_top))
        end_y = int((float(height) / 100.00) * float(self.crop_bottom))

        # to prevent broken data
        if start_y < 0 or end_y <= 0 or start_y >= end_y:
            raise ValueError(
                'Cropping image not possible. Wrong Proportions: start_y=%s, end_y=%s' % (start_y, end_y))

        # width
        width = image.shape[1]
        start_x = 0
        end_x = int(width)

        return image[start_y:end_y, start_x:end_x]

    def resize_img(self, image):
        return resize(image, (self.img_width, self.img_height), interpolation=INTER_CUBIC)

    def transform_img_for_backend(self, image_data):
        height = image_data.shape[0]
        if k.image_dim_ordering() == 'th':
            # Theano-Backend:
            image_data = np.reshape(image_data, (self.img_depth, height, self.img_width))
        else:
            # Tensorflow-Backend
            image_data = np.reshape(image_data, (height, self.img_width, self.img_depth))

        return image_data
