#!/usr/bin/env python
from json import load
from os import path
from posixpath import normpath

import numpy as np
from gym.spaces import Box
from keras import backend as k
from keras.initializers import RandomUniform, VarianceScaling
from keras.layers import Conv2D, Dense, Dropout, Flatten, Input, concatenate  # ,add
from keras.layers.core import Reshape
from keras.models import Model
from keras.optimizers import Adam
from keras.regularizers import l2
from rl.agents import DDPGAgent
from rl.memory import SequentialMemory
from rl.random import OrnsteinUhlenbeckProcess


class Agent:
    def __init__(self, config, image_processor, processor=None, learning_rate_actor=.0001, learning_rate_critic=.001,
                 verbose=0):
        """
        TODO
        :param config: file name without extension
        :param image_processor: file name without extension
        :param processor: GazeboProcessor
        :param learning_rate_critic: float
        :param verbose: integer
        """

        # Agent settings
        tmp_dir = path.dirname(__file__)
        filename = "configs/agent/%s.json" % (config,)
        filename = normpath(path.join(tmp_dir, "..", filename))
        if path.isfile(filename):
            with open(filename) as data_file:
                config = load(data_file)
        else:
            raise IOError('No such file: %s' % (filename,))

        self.agent = None
        self.name = config["name"]

        self.color_scale = config["color_scale"]
        self.img_width = config["img_width"]
        self.img_height = config["img_height"]
        self.img_depth = config["img_depth"]

        if not image_processor.crop_image:
            img_height = config["img_height"]
        else:
            img_height = image_processor.get_cropped_height()

        # Input Shape
        if k.image_dim_ordering() == 'th':
            # Theano-Backend
            self.input_shape = (self.img_depth, img_height, self.img_width)
            self.graph = None
        else:
            # Tensorflow-Backend
            self.input_shape = (img_height, self.img_width, self.img_depth)
            self.graph = k.get_session().graph

        action_space_lows = np.array(
            [config["action_space"]["steer"]["low"],
             # config["action_space"]["speed"]["low"]
             ])
        action_space_lows.astype('float32')

        action_space_highs = np.array(
            [config["action_space"]["steer"]["high"],
             # config["action_space"]["speed"]["high"]
             ])
        action_space_highs.astype('float32')

        observation_space_lows = config["observation_space"]["image"]["low"]
        observation_space_highs = config["observation_space"]["image"]["high"]

        self.action_space = Box(low=action_space_lows, high=action_space_highs)  # steer, speed
        self.observation_space = Box(low=observation_space_lows, high=observation_space_highs,
                                     shape=self.input_shape)  # image

        self.learning_rate_actor = learning_rate_actor
        self.learning_rate_critic = learning_rate_critic
        self.nb_steps_warmup_actor = config["nb_steps_warmup_actor"]
        self.nb_steps_warmup_critic = config["nb_steps_warmup_critic"]
        self.batch_size = config["batch_size"]
        self.verbose = verbose

        self.processor = processor

        self._create_agent()

    def _create_agent(self):
        """
        Creates keras-rl DDPGAgent.
        :return: compiled keras-rl DDPGAgent
        """

        nb_actions = self.action_space.shape[0]

        # Lillicrap et al.
        actor = self._create_actor()
        critic, critic_action_input = self._create_critic(nb_actions=nb_actions)

        # Modified NVIDIA
        # actor = self._nvidia_actor()
        # critic, critic_action_input = self._nvidia_critic(nb_actions=nb_actions)

        memory = SequentialMemory(limit=100000, window_length=1)

        random_process = OrnsteinUhlenbeckProcess(size=nb_actions, theta=.15, mu=0., sigma=.2)

        self.agent = DDPGAgent(nb_actions=nb_actions, actor=actor, critic=critic,
                               critic_action_input=critic_action_input,
                               memory=memory,
                               nb_steps_warmup_critic=self.nb_steps_warmup_critic,
                               nb_steps_warmup_actor=self.nb_steps_warmup_actor,
                               random_process=random_process,
                               gamma=.99,
                               target_model_update=.001,
                               processor=self.processor,
                               batch_size=self.batch_size)
        self.agent.compile(
            (Adam(lr=self.learning_rate_actor, clipnorm=1.), Adam(lr=self.learning_rate_critic, clipnorm=1.)),
            metrics=['mse'])

    def _create_actor(self):
        """
        Creates actor model.
        :return: keras model
        """

        S = Input(shape=(1,) + self.observation_space.shape, name='observation_input')
        S_reshape = Reshape(self.observation_space.shape)(S)
        c1 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug1', padding="valid",
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(
            S_reshape)
        c2 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug2',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c1)
        c3 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', name='debug3',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c2)
        c3_flatten = Flatten(name='flattened_observation')(c3)
        d1 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform', name='debug5')(c3_flatten)
        d2 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform', name='debug6')(d1)
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d2)
        Speed = Dense(units=1,
                      activation='sigmoid',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=0.0, maxval=3e-4)
                      )(d2)
        V = concatenate([Steer, Speed], name='merge_concatenate')
        model = Model(inputs=S, outputs=Steer)  # TODO use 'V' once multi output is supported by keras-rl

        if self.verbose > 0:
            print (model.summary())

        return model

    def _create_critic(self, nb_actions=None):
        """
        Creates critic model.
        :param nb_actions: integer
        :return: keras Model and action input (keras Input)
        """

        S = Input(shape=(1,) + self.observation_space.shape, name='observation_input')
        S_reshape = Reshape(self.observation_space.shape)(S)
        c1 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu', padding="valid",
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(S_reshape)
        c2 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c1)
        c3 = Conv2D(filters=32, kernel_size=(4, 4), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c2)
        observation_flattened = Flatten(name='flattened_observation')(c3)
        O = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform',
                  kernel_regularizer=l2(0.01))(observation_flattened)

        A = Input(shape=self.action_space.shape, name='action_input')

        # TODO activate once Speed is activated
        # a1 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform',
        #            kernel_regularizer=l2(0.01))(A)
        # h1 = add([O,a1], name='merge_sum')

        # TODO use upper h1 instead of this one with Speed activated
        h1 = concatenate([O, A], name='merge_concatenate')
        h2 = Dense(units=200, activation='relu', kernel_initializer='glorot_uniform',
                   kernel_regularizer=l2(0.01))(h1)
        V = Dense(units=nb_actions,
                  activation='linear',
                  bias_initializer='zeros',
                  kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4),
                  kernel_regularizer=l2(0.01)
                  )(h2)
        model = Model(inputs=[S, A], outputs=V)

        if self.verbose > 0:
            print (model.summary())

        return model, A

    def load_weights(self):
        if not isinstance(self.agent, DDPGAgent):
            raise TypeError('Excpeted type DDPGAgent. Got %s instead.' % (type(self.agent),))

        if self.name is None:
            raise TypeError('Expected type String. Got None instead.')

        tmp_dir = path.dirname(__file__)
        file_name = "%s_%s" % (k.image_dim_ordering(), self.name,)

        actor = "trained_data/%s_actor.hdf5" % (file_name,)
        actor = path.join(tmp_dir, "../..", actor)
        actor = normpath(actor)

        critic = "trained_data/%s_critic.hdf5" % (file_name,)
        critic = path.join(tmp_dir, "../..", critic)
        critic = normpath(critic)

        if path.isfile(actor) and path.isfile(critic):
            file_path = "trained_data/%s.hdf5" % (file_name,)
            file_path = normpath(path.join(tmp_dir, "../..", file_path))
            self.agent.load_weights(file_path)
        elif not path.isfile(actor):
            raise IOError('No such file: %s' % (actor,))
        elif not path.isfile(critic):
            raise IOError('No such file: %s' % (critic,))

    def train(self, env=None, nb_steps=50000, nb_max_episode_steps=2000, nb_episodes_test=20, action_repetition=1,
              verbose=0):

        # TODO callback for save depending on loss value?
        self.agent.fit(env,
                       nb_max_episode_steps=nb_max_episode_steps,
                       nb_steps=nb_steps,
                       action_repetition=action_repetition,
                       visualize=False,
                       verbose=verbose)

        # save final weights
        tmp_dir = path.dirname(__file__)
        file_path = "trained_data/%s_%s.hdf5" % (k.image_dim_ordering(), self.name,)
        file_path = normpath(path.join(tmp_dir, "../..", file_path))
        self.agent.save_weights(file_path, overwrite=True)

        # evaluate algorithm
        return self.agent.test(env,
                               nb_episodes=nb_episodes_test,
                               visualize=False,
                               nb_max_episode_steps=nb_max_episode_steps)

    def predict_on_batch(self, x):
        if not isinstance(self.agent, DDPGAgent):
            raise TypeError('Excpeted type DDPGAgent. Got %s instead.' % (type(self.agent),))

        if self.processor:
            x = self.processor.process_observation(x)

        return self.agent.forward(x)

    def make_stateful_predictor(self):
        return lambda x: self.predict_on_batch(x)

    def _nvidia_actor(self):
        S = Input(shape=(1,) + self.observation_space.shape, name='observation_input')
        S_reshape = Reshape(self.observation_space.shape)(S)
        c1 = Conv2D(activation='relu', filters=8, kernel_size=(5, 5), padding="valid", name='debug1',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(S_reshape)
        c2 = Conv2D(filters=12, kernel_size=(5, 5), activation='relu', name='debug2',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c1)
        c3 = Conv2D(filters=16, kernel_size=(5, 5), activation='relu', name='debug3',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c2)
        c4 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug4',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c3)
        c5 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu', name='debug5',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'))(c4)
        observation_flattened = Flatten(name='flattened_observation')(c5)
        d1 = Dense(units=256, activation='relu', name='debug7',
                   kernel_initializer='glorot_uniform')(observation_flattened)
        d1_drop = Dropout(0.2)(d1)
        d2 = Dense(units=100, activation='relu', name='debug8',
                   kernel_initializer='glorot_uniform')(d1_drop)
        d2_drop = Dropout(0.2)(d2)
        d3 = Dense(units=50, activation='relu', name='debug9',
                   kernel_initializer='glorot_uniform')(d2_drop)
        d3_drop = Dropout(0.2)(d3)
        d4 = Dense(units=10, activation='relu', name='debug10',
                   kernel_initializer='glorot_uniform')(d3_drop)
        d4_drop = Dropout(0.2)(d4)
        Steer = Dense(units=1,
                      activation='tanh',
                      name='prediction',
                      bias_initializer='zeros',
                      kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4)
                      )(d4_drop)
        # TODO
        # create Speed V with sigmoid
        # create V as merge of [Steer, Speed] once working

        model = Model(inputs=S, outputs=Steer)

        if self.verbose > 0:
            print (model.summary())

        return model

    def _nvidia_critic(self, nb_actions=None):
        """
        Creates critic model.
        :param nb_actions: integer
        :return: keras Model and action input (keras Input)
        """

        S = Input(shape=(1,) + self.observation_space.shape, name='observation_input')
        S_reshape = Reshape(self.observation_space.shape)(S)
        c1 = Conv2D(activation='relu', filters=8, kernel_size=(5, 5), padding="valid",
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(S_reshape)
        c2 = Conv2D(filters=12, kernel_size=(5, 5), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c1)
        c3 = Conv2D(filters=16, kernel_size=(5, 5), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c2)
        c4 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c3)
        c5 = Conv2D(filters=24, kernel_size=(3, 3), activation='relu',
                    kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                    kernel_regularizer=l2(0.01))(c4)
        observation_flattened = Flatten(name='flattened_observation')(c5)
        O = Dense(units=256, activation='relu',
                  kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
                  kernel_regularizer=l2(0.01))(observation_flattened)

        A = Input(shape=self.action_space.shape, name='action_input')

        # TODO activate once Speed is activated
        # a1 = Dense(units=256, activation='relu',
        #            kernel_initializer=VarianceScaling(mode='fan_in', distribution='uniform'),
        #            kernel_regularizer=l2(0.01))(A)
        # h1 = add([O,a1], name='merge_sum')

        # TODO use upper h1 instead of this one with Speed activated
        h1 = concatenate([O, A], name='merge_concatenate')
        d1 = Dense(units=256, activation='relu',
                   kernel_initializer='glorot_uniform',
                   kernel_regularizer=l2(0.01))(h1)
        d1_drop = Dropout(0.2)(d1)
        d2 = Dense(units=100, activation='relu',
                   kernel_initializer='glorot_uniform',
                   kernel_regularizer=l2(0.01))(d1_drop)
        d2_drop = Dropout(0.2)(d2)
        d3 = Dense(units=50, activation='relu',
                   kernel_initializer='glorot_uniform',
                   kernel_regularizer=l2(0.01))(d2_drop)
        d3_drop = Dropout(0.2)(d3)
        d4 = Dense(units=10, activation='relu',
                   kernel_initializer='glorot_uniform',
                   kernel_regularizer=l2(0.01))(d3_drop)
        d4_drop = Dropout(0.2)(d4)
        V = Dense(units=nb_actions,
                  activation='linear',
                  bias_initializer='zeros',
                  kernel_initializer=RandomUniform(minval=-3e-4, maxval=3e-4),
                  kernel_regularizer=l2(0.01)
                  )(d4_drop)
        model = Model(inputs=[S, A], outputs=V)

        if self.verbose > 0:
            print (model.summary())

        return model, A
