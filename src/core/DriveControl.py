#!/usr/bin/env python
from ackermann_msgs.msg import AckermannDriveStamped


class DriveControl(object):
    def __init__(self):
        self.max_speed = None
        self.zero_speed = None
        self.zero_steering_angle = None

    def create_command(self, steer=0.0, speed=0.0):
        raise NotImplementedError

    def stop_engine(self):
        raise NotImplementedError

    @staticmethod
    def _scale_steer(steer=0.0):
        """
        Limits values to range -1.0 to 1.0
        :param steer: float
        :return: float
        """
        if steer > 1.0:
            steer = 1.0
        if steer < -1.0:
            steer = -1.0

        return steer


class DriveControlSimulation(DriveControl):
    def __init__(self, max_speed=0.0):
        super(DriveControlSimulation, self).__init__()
        self.max_speed = max_speed  # speed in m/s
        self.zero_speed = 0.0
        self.zero_steering_angle = 0.0

    def create_command(self, steer=0.0, speed=0.0):
        a_d_s = AckermannDriveStamped()
        a_d_s.drive.steering_angle = self._scale_steer(steer)  # from -1 to 1
        a_d_s.drive.steering_angle_velocity = 0.0
        a_d_s.drive.speed = self._scale_speed(speed)  # from 0 to 1
        a_d_s.drive.acceleration = 0.0
        a_d_s.drive.jerk = 0.0

        return a_d_s

    def _scale_speed(self, speed=0.0):
        """
        Returns speed in meters per second.
        :param speed: float between 0 and 1
        :return: float
        """
        return self.max_speed * speed

    def stop_engine(self):
        a_d_s = AckermannDriveStamped()
        a_d_s.drive.steering_angle = self.zero_steering_angle
        a_d_s.drive.steering_angle_velocity = 0.0
        a_d_s.drive.speed = self._scale_speed(self.zero_speed)
        a_d_s.drive.acceleration = 0.0
        a_d_s.drive.jerk = 0.0

        return a_d_s
