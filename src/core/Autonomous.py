#!/usr/bin/env python
from time import localtime

# import threading
import numpy as np
import rospy
from DriveControl import DriveControlSimulation
from ackermann_msgs.msg import AckermannDriveStamped
from keras import backend as k
from sensor_msgs.msg import CompressedImage
from utils.file_writer import write_log_text


class Autonomous:
    def __init__(self, agent_class, verbose=0):
        self.drive_control = None
        self.drive_control_publisher = None
        self.agent_class = agent_class
        self.active = False
        self.verbose = verbose

    def _callback(self, cmprs_img_msg, args):
        if self.active:
            predict = args['predict']

            if k.image_dim_ordering() == 'th':
                # Theano-Backend:
                # TODO test if theano's prediction is working correctly
                steer = np.squeeze(predict(cmprs_img_msg))
                # TODO Speed
                # steer, speed = np.squeeze(predict(image_data))
                speed = 1.0  # just for static tests, will be predicted in future
            else:
                # Tensorflow-Backend
                with self.agent_class.graph.as_default():
                    steer = np.squeeze(predict(cmprs_img_msg))
                    # TODO speed
                    # steer, speed = np.squeeze(predict(image_data))
                    speed = 1.0  # just for static tests, will be predicted in future

            command = self.drive_control.create_command(steer=steer, speed=speed)
            self.drive_control_publisher.publish(command)

            if self.verbose == 1:
                rospy.loginfo("steering: %s" % (steer,))
                rospy.loginfo("speed: %s" % (speed,))
            elif self.verbose == 2:
                write_log_text(name=self.agent_class.agent_name,
                               log_text="(%s) - steering: %s, speed: %s" % (localtime(), steer, speed,),
                               mode="autonomous")

    def set_state(self, active=False):
        self.active = active

    def stop(self):
        self.active = False

        if self.drive_control is not None and self.drive_control_publisher is not None:
            command = self.drive_control.stop_engine()
            self.drive_control_publisher.publish(command)

    def start(self, queue_size=100, max_speed=0.0, wait_for_input=True):
        self.drive_control = DriveControlSimulation(max_speed)
        cam_path = "camera/zed/rgb/image_rect_color/compressed"
        output_path = 'vesc/ackermann_cmd_mux/input/navigation'
        drive_control_publisher = rospy.Publisher(output_path,
                                                  AckermannDriveStamped,
                                                  queue_size=queue_size)

        if drive_control_publisher is not None:
            self.drive_control_publisher = drive_control_publisher
        else:
            raise ValueError("No DriveControl Publisher defined.")

        rospy.init_node('neuroracer_ai', anonymous=True)

        rospy.Subscriber(cam_path,
                         CompressedImage,
                         self._callback,
                         callback_args={
                             'predict': self.agent_class.make_stateful_predictor()
                         })

        rospy.on_shutdown(self.stop)

        if wait_for_input:
            raw_input("Press Enter to start")

        self.active = True

        rospy.spin()
