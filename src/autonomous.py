#!/usr/bin/env python
from json import load
from os import path
from posixpath import normpath
from sys import argv

# from utils.file_writer import write_nn_structure
from core.Agent import Agent
from core.Autonomous import Autonomous
from processors.GazeboProcessor import GazeboProcessor
from processors.ImageProcessor import ImageProcessor
from processors.VisualizationProcessor import VisualizationProcessor


def main():
    config_agent = 'default_bgr'  # default_bgr/default_mono8
    config_img_processing = 'default_bgr'  # default_bgr/default_mono8
    config_env_processing = 'gazebo'
    config_run = 'default'

    # TODO use argparse
    # overwrite default config if args are parsed
    if argv[1:]:
        if argv[1:][0]:
            config_agent = argv[1:][0]
        if argv[1:][1]:
            config_img_processing = argv[1:][1]
        if argv[1:][2]:
            config_env_processing = argv[1:][2]
        if argv[1:][3]:
            config_run = argv[1:][3]

    tmp_dir = path.dirname(__file__)
    file_path = "configs/run/autonomous/%s.json" % (config_run,)
    file_path = normpath(path.join(tmp_dir, file_path))
    if path.isfile(file_path):
        with open(file_path) as data_file:
            config_run = load(data_file)
    else:
        raise IOError('No such file: %s' % (file_path,))

    file_path = "configs/env_processing/%s.json" % (config_env_processing,)
    file_path = normpath(path.join(tmp_dir, file_path))
    if path.isfile(file_path):
        with open(file_path) as data_file:
            config_env_processing = load(data_file)
    else:
        raise IOError('No such file: %s' % (file_path,))

    image_processor = ImageProcessor(config=config_img_processing,
                                     config_agent=config_agent)

    env_processor = GazeboProcessor(image_processor=image_processor,
                                    record=config_env_processing['record']['image'])

    agent_class = Agent(config=config_agent,
                        image_processor=image_processor,
                        processor=env_processor,
                        verbose=config_run['verbose']['agent'])
    agent_class.load_weights()

    # write_nn_structure(agent_class.agent.actor, "%s_actor" % (agent_class.name,))
    # write_nn_structure(agent_class.agent.critic, "%s_critic" % (agent_class.name,))

    visualizer = VisualizationProcessor(visualize=config_env_processing['record']['prediction'],
                                        actor=agent_class.agent.actor)
    env_processor.set_visualizer(visualizer)

    autonomous = Autonomous(agent_class=agent_class, verbose=config_run['verbose']['autonomous'])
    autonomous.start(max_speed=config_run['max_speed'],
                     wait_for_input=config_run['wait_for_input'])


if __name__ == '__main__':
    main()
