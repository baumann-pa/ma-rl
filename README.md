### `Note:`
This repository will not receive any further updates. New repo adress will be provided soon. 


### Deep Reinforcement Learning to control a Racecar-Simulation with Deep Deterministic Policy Gradient in Countinous Action Space

1. Execute setup.py
2. Customize parameter of src/configs/
3. Execute src/train.py, respectively src/autonomous.py

### Anmerkung:
src/ folder has to be added to the PYTHONPATH:
export PYTHONPATH="${PYTHONPATH}:/path/src"

Python 2.7, Gazebo 8.1 and ROS lunar have been used.

Tensorflow has been used as backend. But Theano is also usable.

### MIT-Komponenten:
* MIT-Racecar-Simulation: https://github.com/mit-racecar/racecar-simulator

* MIT-Racecar: https://github.com/mit-racecar/racecar

* VESC: https://github.com/mit-racecar/vesc

### Alternatively the workspace, which was used (includes all three required components):
* racecar-ws: http://home.htw-berlin.de/~s0547175/racecar-ws.zip
