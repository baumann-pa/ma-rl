#!/usr/bin/env python
import os

if __name__ == '__main__':
    # creates folder for trained data
    trained_data = os.path.join(os.path.dirname(__file__), 'trained_data')
    if not os.path.exists(trained_data):
        os.makedirs(trained_data)

    # creates folder for graphs
    graphs = os.path.join(os.path.dirname(__file__), 'graphs')
    if not os.path.exists(graphs):
        os.makedirs(graphs)

    # creates folder for logs
    logs = os.path.join(os.path.dirname(__file__), 'logs')
    if not os.path.exists(logs):
        os.makedirs(logs)

    # creates folder for vis
    records = os.path.join(os.path.dirname(__file__), 'vis')
    if not os.path.exists(records):
        os.makedirs(records)
    # subfolder raw
    raw = "%s/raw" % (records,)
    if not os.path.exists(raw):
        os.makedirs(raw)
    # subfolder vis
    vis = "%s/layer" % (records,)
    if not os.path.exists(vis):
        os.makedirs(vis)
